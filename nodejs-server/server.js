const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const morgan = require('morgan');
const actuator = require('express-actuator');
const cors = require('cors');

const _constants = require('./config/constants');
const _routes = require('./routes/routes');
const _database = require('./config/database');
const _utils = require('./services/Util.service');


const app = express();
app.use(cors());
app.use(actuator());


app.use(helmet());
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(_constants.INITIAL_PATH, _routes);

(async() => {
  let connected = await _database.connect();
  if(connected){
    app.listen(_constants.PORT_SERVER, () => {
      console.log(`Server connected in ${_constants.PORT_SERVER}`);
      _utils.getNewsFromAPI();
      setInterval(() => {
        _utils.getNewsFromAPI();
      }, 3600000);
    });
  }
  
})();


module.exports = app;
