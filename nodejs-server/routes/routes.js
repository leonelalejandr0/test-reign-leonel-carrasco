const express = require('express');
const NewsController = require('../controllers/News.controller');

let app = express();

let router = express.Router();
router.route('/news/:id?')
.get(NewsController().getNews)
.delete(NewsController().deleteNews);

app.use(router);

module.exports = app;