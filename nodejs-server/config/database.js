const mongoose = require('mongoose');
const _constants = require('./constants');


exports.connect = async() => {
    return await mongoose
    .connect(_constants.MONGO_URI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false 
    })
    .then(() =>{
        console.log('MongoDB connected!');
        return true;
    })
    .catch(err => {
        console.log(err);
        return false;
    });
}

