const axios = require('axios');
const _constants = require('../config/constants');

const NewsModel = require('../models/News.model');

module.exports = {
    getNewsFromAPI : () => {
        console.log('Loading data from API');
        
        axios.get(_constants.URL_API).then((response) => {
            NewsModel.insertMany(response.data.hits, { ordered : false });
        });
    }
}