const _config = require('../config/app.config');

module.exports = {
    getUrlGetToken : () => {
        let endpoints = _config.endpoints;
        let url = `${endpoints.baseUrl}${endpoints.login_token}`;
        let mapObj = {
            "{user_token}" : endpoints.user_token,
            "{passwd_token}" : endpoints.passwd_token
        };
        return replaceAll(url,mapObj);
    },
    getUrlGetReasons : (token) => {
        let endpoints = _config.endpoints;
        let url = `${endpoints.baseUrl}${endpoints.reasons}`;
        let mapObj = {
            "{token}" : token
        };
        return replaceAll(url,mapObj);  
        
    },
    getUrlGetComplaints : (token, lead_id) => {
        let endpoints = _config.endpoints;
        let url = `${endpoints.baseUrl}${endpoints.get_complaints}`;
        let mapObj = {
            "{body}" : lead_id ? `&lead_id=${lead_id}` : '',
            "{token}" : token
        };
        return replaceAll(url,mapObj);  
    },
    getUrlAddComplaint : (complaint, token) => {
        let endpoints = _config.endpoints;
        let query = `lead_id=${complaint.lead_id}&complaint=${complaint.complaint}&status=${complaint.status}&reason_id=${complaint.reason_id}&created_by=${complaint.created_by}&create_date=${complaint.create_date}`;
        let url = `${endpoints.baseUrl}${endpoints.add_complaint}`;
        let mapObj = {
            "{body}" : query,
            "{token}" : token
        };
        return replaceAll(url,mapObj);    
        
    },
    getUrlUpdateComplaint : (complaint, token) => {
        let endpoints = _config.endpoints;
        let query = `complaint_id=${complaint.complaint_id}&complaint=${complaint.complaint}&status=${complaint.status}&created_by=${complaint.created_by}&update_date=${complaint.update_date}`;
        let url = `${endpoints.baseUrl}${endpoints.update_complaint}`;
        let mapObj = {
            "{body}" : query,
            "{token}" : token
        };
        return replaceAll(url,mapObj);    
    }
};


let replaceAll = (str, mapObj) => {
    var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
    return str.replace(re, function(matched){
        return mapObj[matched.toLowerCase()];
    });
};

