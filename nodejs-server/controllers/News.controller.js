
const _ = require('lodash');
const statusCode = require('http-status-codes');
const NewsModel = require('../models/News.model');


module.exports = () => ({

    getNews : (req, res) => {
        NewsModel.find({deleted : false, story_id : { $ne: null }}, '-deleted -_id -__v').sort('-created_at').then((results) => {
            if(results.length)
                return res.status(statusCode.OK).json(results);
            else
                return res.sendStatus(statusCode.NO_CONTENT);
        });

    },
    deleteNews : (req, res) => {
        if(req.params.id){
            NewsModel.findOneAndUpdate({
                story_id : req.params.id
            },{
                deleted : true
            }).then((result) => {
                if(result)
                    return res.sendStatus(statusCode.OK);
            });
        }else{
            return res.sendStatus(statusCode.BAD_REQUEST);
        }
    }

});


