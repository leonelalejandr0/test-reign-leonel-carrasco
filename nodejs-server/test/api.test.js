const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';

describe('GET - News',()=>{
    it('should return all news', (done) => {
        chai.request(url)
        .get('/api/news')
        .end( (err,res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            done();
        });
    });
});

describe('DELETE - News',()=>{
    it('should return BAD REQUEST if not contains param story_id', (done) => {
        chai.request(url)
        .delete('/api/news')
        .end( (err,res) => {
            expect(res).to.have.status(400);
            done();
        });
    });

    it('should return OK if it was deleted with success', (done) => {
        chai.request(url)
        .get('/api/news')
        .end( (err,data) => {
            const news = data.body[0];
            chai.request(url)
            .delete('/api/news/' + news.story_id)
            .end( (err,res) => {
                expect(res).to.have.status(200);
                done();
            });
        });
        
    });
});
   