const mongoose = require('mongoose');
const _ = require('lodash');

const newsSchema = new mongoose.Schema({
  story_id: {
    type : String,
    unique : true
  },
  title : String,
  story_title : String,
  author: String,
  created_at: Date,
  story_url: String,
  url: String,
  deleted: {
    type : Boolean,
    default : false
  }
},{
  versionKey : false,
  
});



const News = mongoose.model('News', newsSchema);

module.exports = News;