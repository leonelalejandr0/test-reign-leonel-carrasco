# Leonel Carrasco - TEST


## Installation

Firstly, you must have [docker](https://www.docker.com/get-started) and [GIT](https://git-scm.com/) installed in your PC.

Second, you must clone this repository

```bash
git@gitlab.com:leonelalejandr0/test-reign-leonel-carrasco.git
```

Third, go to project's root folder open a CLI and execute 

```bash
docker-compose up --build
```
Please wait, this take a few minutes

## Usage

Once finishing the installation, open Chrome/Firefox/Edge and write the follow URL http://localhost:4200 
