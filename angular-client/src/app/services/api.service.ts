import { Injectable } from '@angular/core';
import { News } from '../models/news_model';
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private API_URL = 'http://localhost:3000/api/news';

  constructor(private http:HttpClient){

  }

  getNews():Observable<News[]>{
    const url = `${this.API_URL}`;
    return this.http.get(url).pipe(
      map((data:any[])=>
        data.map(
          (item:any) => 
            new News(item.story_id,item.story_title,item.story_url,item.url,item.title,item.author,item.created_at)
        )
      )
    );
  }

  deleteNews(n:News):Observable<any>{
    const url = `${this.API_URL}/${n.story_id}`;
    return this.http.delete(url,{responseType : 'text', observe : 'response'});
  }
}
