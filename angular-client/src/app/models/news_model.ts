export class News{
    

    constructor(
        public story_id: String,
        public story_title: String,
        public story_url:String,
        public url:String,
        public title: String,
        public author: String,
        public created_at: Date
    ){

    }
}