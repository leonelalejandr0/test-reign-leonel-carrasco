import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/news_model';
import { ApiService } from '../services/api.service';
import * as _ from 'lodash';


@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.css']
})
export class ListNewsComponent implements OnInit {

  listNews:News[] = [];

  constructor(private _apiService:ApiService) { 
    this.fillList();
  }

  ngOnInit(): void {
  }

  fillList(){

    this._apiService.getNews().subscribe((results) => {     
      this.listNews = results;
    });

  }

  deleteNews(n:News, event){
    event.preventDefault();
    event.stopPropagation();
    this._apiService.deleteNews(n).subscribe((result) => {
      if(result.status)
        _.remove(this.listNews,{story_id : n.story_id});
    });
    
  }

  goToLink(n:News){
    const url = (n.story_url || n.url).toString();
    window.open(url, '_blank');
  }

}
